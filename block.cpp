#include "block.h"

Block::Block(bool mine_flag, QWidget* parent):QLabel(parent){
    isMine = mine_flag;
    correctlyMarked = false;
    flagged = false;
    number = 2;
    setPixmap(QPixmap(":/blocks/img/block_normal.png"));
}

void Block::setNumber(int number){ this->number = number; }

bool Block::is_Mine() const { return isMine; }

void Block::floodfilled() {
    setPixmap(QPixmap(":/blocks/img/block_"+QString("%1").arg(number)+".png"));
    update();
    correctlyMarked = true;
}
bool Block::markedCorrect() const{
    return correctlyMarked;
}

void Block::mousePressEvent(QMouseEvent* event){
    if(event->button()==Qt::LeftButton){
        if(flagged == false){
            if(isMine){
                setPixmap(QPixmap(":/blocks/img/block_explode.png"));
                update();
                emit explode();
            } else { 
                setPixmap(QPixmap(":/blocks/img/block_" + QString("%1").arg(number)+".png"));
                update();
                if(!correctlyMarked) emit safe();
                correctlyMarked = true;
            }
           }
        } else if(event->button()==Qt::RightButton){
                if(!correctlyMarked)
                if(flagged == false){
                        flagged = true;
                        setPixmap(QPixmap(":/blocks/img/block_flagged.png"));
                } else {
                        flagged = false;
                        setPixmap(QPixmap(":/blocks/img/block_normal.png"));
                }
                    update();
        }
}

