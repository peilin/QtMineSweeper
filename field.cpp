#include "field.h"
#include <QDebug>
field::field(QWidget* parent):QWidget(parent)
{
   correctBlocks = 0;
}

void field::init(int row, int col){
    this->row = row;
    this->col = col;
    this->totalBlocks = row * col;
    this->totalMines = row * col / 10;

    bool* mineFlag = new bool[totalBlocks];
    for(int i = 0; i < totalMines; i++) mineFlag[i]=true;
    for(int i = totalMines; i < totalBlocks; i++) mineFlag[i]=false;
    std::random_shuffle(mineFlag, mineFlag + totalBlocks);

    gridLayout = new QGridLayout(this);
    for(int i = 0; i < row; i++)
        for(int j = 0;j < col; j++)
            gridLayout->addWidget(new Block(mineFlag[i*col + j]), i, j);

    for(int i = 0; i < row ; i++)
        for(int j = 0; j < col; j++) {
            Block* current_block = static_cast<Block*>(gridLayout->itemAtPosition(i,j)->widget());
            current_block->setNumber(calculateMines(i,j));
            connect(current_block,SIGNAL(safe()),this,SLOT(slotSafe()));
            connect(current_block,SIGNAL(explode()),this,SLOT(slotExplode()));
    }

    setFixedSize(sizeHint());
}


void field::slotSafe(){
    ++ correctBlocks;
    if(correctBlocks == totalBlocks - totalMines)
        QMessageBox::information(this,tr("GOOD JOB"),tr("You win!\nPlease Restart"));
    int count = 0;
    for(int i = 0; i < row ; i++)
        for(int j = 0; j < col; j++)
            if(static_cast<Block*>(gridLayout->itemAtPosition(i,j)->widget())->markedCorrect()) count ++;
    qDebug() << count;
}

void field::slotExplode() {
    QMessageBox::information(this,tr("BOOM!"),tr("You Lost!"));
}

int field::calculateMines(int x, int y) const {
    int numMines = 0;
    for(int i = 0; i < 3; i ++)
        for(int j=0;j<3; j ++)
            if((x - 1 + i >= 0) && (x - 1 + i < row) && (y - 1 + j >= 0) && ( y - 1 + j < col))
                if (isMineAt(x - 1 + i,y - 1 + j)) ++ numMines;
    return numMines;
}

bool field::isMineAt(int x, int y) const {
    return static_cast<Block*>(gridLayout->itemAtPosition(x, y)->widget())->is_Mine();
}

void field::press(int x, int y) {
    //qDebug() << "filled";
    static_cast<Block*>(gridLayout->itemAtPosition(x, y)->widget())->floodfilled();
}

void field::floodFill( int x, int y ) {
   if (!static_cast<Block*>(gridLayout->itemAtPosition(x, y)->widget())->is_Mine()
           && x >= 0 && y >= 0 && x < row && y < col) {
        qDebug() << "filled";
       press(x, y);
       if (!static_cast<Block*>(gridLayout->itemAtPosition(x+1, y)->widget())->is_Mine()
               && !static_cast<Block*>(gridLayout->itemAtPosition(x+1, y)->widget())->markedCorrect())
           press(x+1, y);
       if (!static_cast<Block*>(gridLayout->itemAtPosition(x-1, y)->widget())->is_Mine()
               && !static_cast<Block*>(gridLayout->itemAtPosition(x-1, y)->widget())->markedCorrect())
           press(x-1, y);
       if (!static_cast<Block*>(gridLayout->itemAtPosition(x, y+1)->widget())->is_Mine()
               && !static_cast<Block*>(gridLayout->itemAtPosition(x, y+1)->widget())->markedCorrect())
           press(x, y+1);
       if (!static_cast<Block*>(gridLayout->itemAtPosition(x, y-1)->widget())->is_Mine()
               && !static_cast<Block*>(gridLayout->itemAtPosition(x, y-1)->widget())->markedCorrect())
           press(x, y-1);

       //floodFill( x + 1, y );
       //floodFill( x - 1, y );
       //floodFill( x, y - 1 );
       //floodFill( x, y + 1 );
   } else {
       return;
   }
}
