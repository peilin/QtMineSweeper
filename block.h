#ifndef BLOCK_H
#define BLOCK_H

#include <QLabel>
#include <QMouseEvent>
#include <QMessageBox>

class Block:public QLabel
{
    Q_OBJECT
public:
    explicit Block(bool mine_flag,QWidget* parent = 0);
    void setNumber(int number);
    bool is_Mine() const;
    void floodfilled();
    bool markedCorrect() const;
signals:
    void explode();
    void safe();
protected:
    void mousePressEvent(QMouseEvent* event);
private:
    bool isMine;
    bool correctlyMarked;
    bool flagged;
    int number;
};

#endif // BLOCK_H
