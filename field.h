#ifndef FIELD_H
#define FIELD_H

#include "block.h"
#include <QGridLayout>
#include <QLabel>

class field : public QWidget {
    Q_OBJECT
public:
    field(QWidget* parent=0);
    void init(int row, int col);
private slots:
    void slotSafe();
    void slotExplode();
private:
    int calculateMines(int x,int y) const;
    void floodFill(int x, int y);
    void press(int x, int y);
    bool isMineAt(int x, int y) const;
    QGridLayout *gridLayout;
    int row;
    int col;
    int totalBlocks;
    int totalMines;
    int correctBlocks;
};

#endif // FIELD_H
