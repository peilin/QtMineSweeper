#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QLabel>
#include "block.h"
#include <QDebug>
#include "field.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void refresh(int row, int col);
    void restart(int row, int col);
private slots:
    void on_actionIntermediate_triggered();

    void on_actionExit_triggered();

    void on_actionSimple_triggered();

    void on_actionHard_triggered();

    void on_actionYoutube_Link_triggered();

    void on_actionGithub_Link_triggered();

private:
    Ui::MainWindow *ui;
    int pixelBlock = 39;
};

#endif // MAINWINDOW_H
