#-------------------------------------------------
#
# Project created by QtCreator 2016-12-20T15:16:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CS368-MineSweeper
TEMPLATE = app


SOURCES += \
    block.cpp \
    main.cpp \
    mainwindow.cpp \
    field.cpp

HEADERS  += \
    block.h \
    mainwindow.h \
    field.h

FORMS    += mainwindow.ui

RESOURCES += \
    image.qrc
