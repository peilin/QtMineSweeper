#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopServices>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->statusBar()->setSizeGripEnabled(false);
    restart(8, 8);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::restart(int row, int col) {
    field* newGame = new field();
    newGame->init(row, col);
    setCentralWidget(newGame);
    adjustSize();
}


void MainWindow::on_actionIntermediate_triggered()
{
    restart(16, 16);
}

void MainWindow::on_actionSimple_triggered()
{
    restart(8, 8);
}

void MainWindow::on_actionHard_triggered()
{
    restart(32, 32);
}

void MainWindow::on_actionExit_triggered()
{

    QMessageBox::information(this,tr("Thank you for playing"),tr("Project by Peilin & Haowei"));
    QCoreApplication::exit();
}


void MainWindow::refresh(int row, int col){
    QWidget* widget = new QWidget();
    QGridLayout* gridLayout = new QGridLayout();
    //gridLayout->setMargin(0);
    bool f = false;
    for(int i = 1; i <= row;i++) {
        for(int j=0; j < col; j++) {
            gridLayout -> addWidget(new Block(f),i,j);
        }
        f = true;
    }

    int var1 = gridLayout->columnCount();
    qDebug() << var1;
    widget->setLayout(gridLayout);
    widget->resize(pixelBlock * row, pixelBlock * col);
    setCentralWidget(widget);

}




void MainWindow::on_actionYoutube_Link_triggered()
{
    QDesktopServices::openUrl(QUrl("http://www.dailymotion.com/anfieldstadium"));
}

void MainWindow::on_actionGithub_Link_triggered()
{
   QDesktopServices::openUrl(QUrl("https://gitlab.com/peilin/QtMineSweeper.git"));
}
